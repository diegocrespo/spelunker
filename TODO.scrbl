1. Figure out how to handle win_err 5 when trying to access directories
  a. maybe just skip these directories b/c I can't peer into them?
2. add a way to ignore certain files (I.E ~ emacs files)
3. Double click to expand selection in text-field for files
4. Right click to dialog window + open option to set folder to whatever folder you opened
  a. update the text-field
  b. update the search folder
5. Highlight folders
6. Add way to cache files
7. Restrict Open Folder to just picking folders
8. Display all folders in a flat hierarchy
9. Add a tab pane for a quick tutorial?
10. Add a time since last cache update line in the app
11. remove hardcoded serialized-data path
12. search against the cached folders and directories
13. on windows that paths should be displayed like they are in windows with C: and backslashes
14. Figure out whether to keep the ALL-FILES and ALL-DIRS as a list of paths or convert to strings